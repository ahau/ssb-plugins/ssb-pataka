# ssb-pataka

A `secret-stack` plugin which provides:
- a graphql API for pataka
- a web-server that hosts a web-registration page

Has dependencies:
- ssb-profile
- ssb-tribes
  - this is only used for encrypting registrations sent from web-form

## Config

```js
{
  pataka: {
    allowedOrigins: [
      // 'http://localhost:8000', // for local access if using default port
      'https://register.whanau.nz',
    ],
    webRegistration: {
      // port: 8000,              // port form is served on
      // tribes: [
      //   '%2Sn8sdDl5+cJqrxo2JdVACReYudCbYQAkB0sRHEZARU=.cloaked'
      // ],
      // httpsProxyPort: 7000,    // single https requests to internal servers
      https: {
        privkey,                  // private key
        fullchain                 // certificate
      }
    }
  },
  // serveBlobs: {
  //   port: 28088
  // }
}
```

You will need to set `allowedOrigins` for your registration page to be allowed
to call your graphql API.

### Web Registration

This feature is only enabled if `config.pataka.webRegistration` is present.

For this to work, you MUST:
1. set up a domain name record
   - e.g. point `register.whanau.nz` to your static IP address
2. aquire https key + cert
3. update the `config` with:
  - `config.pataka.allowedOrigins`
     - e.g. `["https://register.whanau.nz"]`
  - `config.pataka.webRegistration.https.privkey` - your https private key (utf-8 encoded)
  - `config.pataka.webRegistration.https.fullchain` - the associated certificate (utf-8 encoded)
4. set up port forwarding on your router
  - map port `443` => `7000`  (or `config.pataka.webRegistration.httpsProxyPort`)

For https keys/ certs we used [LetsEncrypt / Certbot](https://eff-certbot.readthedocs.io/en/latest/using.html#manual):
- install certbot
- run: 
  ```bash
  sudo certbot certonly --manual --preferred-challenges dns
  ```
- create DNS TXT records, check with tools then proceed

NOTE these only last for 90 days (manual for remote setup is hard, and also if your DNS provider doesn't have a nice certbot plugin it's hard)
TODO figure out a nice way to auto-update.

Optionally, if you want your web registration to only offer forms for *some* tribes,
you can set `config.pataka.webRegistration.tribes` to be an Array of tribeIds. 
Further, if there is only one tribe listed, users will be automatically routed to the
registration page of that tribe.

### ENV

`AHAU_LOGGING=true` turns on verbose logging


## Development

Setup:
```bash
npm i
cd ui && npm i
```

In different terminal windows:
1. `cd ui && npm run dev` (wait for this to be ready)
2. `npm run dev` (run this is root of this project)

This will start up a pataka in development mode, and log out an invite-code.
NOTE it uses the `ahau-pataka-dev` user, so that you can easily piggy-back
data and connections set up using `pataka` dev you have been doing, and data
is persisted.

### Developing the web-registration form

Assuming `ssb-pataka` Development setup is already running, do the following:
1. start up Āhau in dev-mode
2. create a group (with custom-fields)
3. if you haven't already, join the dev Pataka you started (see invite code printed at startup)
4. browse the web-reg form at  http://localhost:8087

If you want to test the support for allowed tribes only:
1. get the `tribeId`
  - browse to tribe profile in UI
  - open dev tools > console, and run
    ```js
    decodeURIComponent(window.location.pathname.split('/')[2])
    ```
2. edit `test/web-ref/test-bot.js` to add that id to `pataka.webRegistration.tribes`
3. start the testbot up
4. use the invite code to join the pataka from dev Āhau
5. browse to http://localhost:8000 (default production location we serve form)


### Testing web-registration serving

If you want to test how it will behave in a production setting, you need to
build the UI, then start a testbot in production mode:
1. `cd ui && npm run build`
2. `cd .. && node test/test-bot.js`

when not required into another file, test-bot starts up a temp instance, and if
`NODE_ENV` is not set then it will run in production mode, serving the built
files.

## Publishing

```bash
npm publish
```

Note that this auto-builds the UI before publishing (see script:
`prepublishOnly`)

